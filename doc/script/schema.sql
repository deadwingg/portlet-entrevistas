DROP TABLE IF EXISTS entrevista;
DROP TABLE IF EXISTS usuario;

CREATE TABLE usuario (
    id BIGINT PRIMARY KEY AUTO_INCREMENT,
    username VARCHAR (15) NOT NULL UNIQUE,
    password VARCHAR (100) NOT NULL,
    enabled TINYINT NOT NULL DEFAULT 1
);

CREATE TABLE entrevista (
    id BIGINT PRIMARY KEY AUTO_INCREMENT,
    nombre_entrevistado VARCHAR (50) NOT NULL,
    fecha_pactada DATETIME,
    id_usuario BIGINT,
    FOREIGN KEY (id_usuario) REFERENCES usuario(id)
);

-- -----------------------------------------------------------------------------
-- usuario
-- -----------------------------------------------------------------------------
INSERT INTO usuario
(username   , password      ) VALUES
('santi'   , '{noop}1234'  ),
('agus', '{noop}1234');

-- -----------------------------------------------------------------------------
-- entrevista
-- -----------------------------------------------------------------------------
INSERT INTO entrevista
(id_usuario, nombre_entrevistado, fecha_pactada        ) VALUES
(2         , 'Leonardo De Seta'   , '2030-03-01 15:19:01'),
(2         , 'Alan Milton Patricio', '2025-03-01 16:19:01'),
(2         , 'Nahuel Burgos'       , '2024-03-01 19:19:01'),
(1         , 'Ivan Gomez'        , '2022-04-01 15:19:01'),
(1         , 'Juan Bori'        , '2023-06-01 14:19:01');