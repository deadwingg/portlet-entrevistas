package com.somospnt.portlet.mvc.domain;

import java.time.ZonedDateTime;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.Future;
import javax.validation.constraints.NotBlank;

@Entity
public class Entrevista {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @NotBlank(message = "Este campo no puede estar vacío")
    private String nombreEntrevistado;
    @Future
    private ZonedDateTime fechaPactada;
    @ManyToOne
    @JoinColumn(name = "id_usuario")
    private Usuario usuario;

    public Entrevista() {
    }
    

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombreEntrevistado() {
        return nombreEntrevistado;
    }

    public void setNombreEntrevistado(String nombreEntrevistado) {
        this.nombreEntrevistado = nombreEntrevistado;
    }

    public ZonedDateTime getFechaPactada() {
        return fechaPactada;
    }

    public void setFechaPactada(ZonedDateTime fechaPactada) {
        this.fechaPactada = fechaPactada;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }
    
}
