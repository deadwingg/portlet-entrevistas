package com.somospnt.portlet.mvc.business.service;

import com.somospnt.portlet.mvc.domain.Usuario;
import com.somospnt.portlet.mvc.repository.UsuarioRepository;
import javax.transaction.Transactional;
import org.springframework.stereotype.Service;


@Service
@Transactional
public class UsuarioService {
    
    private final UsuarioRepository usuarioRepository;

    public UsuarioService(UsuarioRepository usuarioRepository) {
        this.usuarioRepository = usuarioRepository;
    }
    
    public Usuario buscarPorUsername(String username) {
        return usuarioRepository.findByUsername(username);
    }
    
}
