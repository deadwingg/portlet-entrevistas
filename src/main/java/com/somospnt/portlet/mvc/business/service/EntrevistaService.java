package com.somospnt.portlet.mvc.business.service;

import com.somospnt.portlet.mvc.domain.Entrevista;
import com.somospnt.portlet.mvc.domain.Usuario;
import com.somospnt.portlet.mvc.repository.EntrevistaRepository;
import java.util.List;
import javax.transaction.Transactional;
import javax.validation.Valid;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;

@Service
@Transactional
@Validated
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class EntrevistaService {

    private final EntrevistaRepository entrevistaRepository;
    private final UsuarioService usuarioService;

    public EntrevistaService(EntrevistaRepository entrevistaRepository, UsuarioService usuarioService) {
        this.entrevistaRepository = entrevistaRepository;
        this.usuarioService = usuarioService;
    }

    public void crear(@Valid Entrevista entrevista) {
        Usuario usuario = usuarioService.buscarPorUsername("santi");
        entrevista.setUsuario(usuario);
        entrevistaRepository.save(entrevista);
    }

    public void eliminarPorId(Long id) {
        entrevistaRepository.deleteById(id);

    }

    public List<Entrevista> buscarTodas() {
        return entrevistaRepository.findAll();
    }

}
