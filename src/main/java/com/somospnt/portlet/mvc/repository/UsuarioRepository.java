package com.somospnt.portlet.mvc.repository;

import com.somospnt.portlet.mvc.domain.Usuario;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UsuarioRepository extends JpaRepository<Usuario, Long> {
    Usuario findByUsername(String username);
}