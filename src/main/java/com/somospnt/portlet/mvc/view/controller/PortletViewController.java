package com.somospnt.portlet.mvc.view.controller;

import com.liferay.portletmvc4spring.bind.annotation.ActionMapping;
import com.liferay.portletmvc4spring.bind.annotation.RenderMapping;
import com.liferay.portletmvc4spring.bind.annotation.ResourceMapping;
import com.somospnt.portlet.mvc.business.service.EntrevistaService;
import com.somospnt.portlet.mvc.domain.Entrevista;
import com.somospnt.portlet.mvc.vo.EntrevistaVo;
import java.util.List;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping("VIEW")
public class PortletViewController {

    private final EntrevistaService entrevistaService;

    public PortletViewController(EntrevistaService entrevistaService) {
        this.entrevistaService = entrevistaService;
    }

    @RenderMapping
    public String vistaPrincipal(Model model) {
        List<Entrevista> entrevistasAgendadas = entrevistaService.buscarTodas();
        EntrevistaVo entrevistaAPactar = new EntrevistaVo();
        model.addAttribute("entrevistaAPactar", entrevistaAPactar);
        model.addAttribute("entrevistasAgendadas", entrevistasAgendadas);
        return "view";
    }

    @ActionMapping
    public void crearEntrevista(@ModelAttribute("entrevistaAPactar") EntrevistaVo entrevistaVo) {
        Entrevista entrevista = entrevistaVo.toEntrevista();
        entrevistaService.crear(entrevista);
    }

    @ResourceMapping
    public void eliminarEntrevista(@RequestParam("entrevistaId") Long id) {
        entrevistaService.eliminarPorId(id);
    }

}
