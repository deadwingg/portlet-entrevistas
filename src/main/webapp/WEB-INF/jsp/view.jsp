<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib uri="http://xmlns.jcp.org/portlet_3_0" prefix="portlet" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page import="com.somospnt.portlet.mvc.domain.Entrevista" %>
<%@ page import="java.time.ZonedDateTime" %>
<%@ page import="java.time.format.DateTimeFormatter" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<portlet:actionURL var="crearEntrevistaURL"/>
<portlet:resourceURL var="eliminarEntrevistaURL"/>


<head>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous"></link>
    <link rel="preconnect" href="https://fonts.gstatic.com"/>
    <link href="https://fonts.googleapis.com/css2?family=Rubik:wght@300;400&display=swap" rel="stylesheet"/>
</head>

<header>
    <nav>
        <div class="bg-dark text-light p-2">
            <h2 class="text-center">Agenda de entrevistas</h2>
        </div>
    </nav>
</header>

<main>
    <div class="container">
        <div class="w-100 mt-3 mb-4">
            <h4>Agende una entrevista</h4>
            <form:form class="row" method="POST" action="${crearEntrevistaURL}" modelAttribute="entrevistaAPactar">
                <div class="col-12 col-sm-4">
                    <form:label class="mb-2" for="nombrecompleto" path="nombre">Nombre y apellido *</form:label>
                    <form:input class="form-control" path="nombre" id="nombrecompleto" type="text" required="required"/>
                </div>  
                <div class="col-12 col-sm-4">
                    <form:label class="mb-2" for="fecha" path="fecha">Fecha</form:label>
                    <form:input class="form-control" path="fecha"  oninput="entrevistas.validarFechaHora()" min="hoy" id="fecha" type="text" onblur="(this.type = 'text')" onfocus="(this.type = 'date')"/>
                </div>
                <div class="col-12 col-sm-2">
                    <form:label class="mb-2" for="hora" path="hora">Horario</form:label>
                    <form:input class="form-control" path="hora" id="hora" type="time"/>
                </div>
                <div class="col-12 col-sm-2 d-flex justify-content-center p-4">
                    <input class="flex-fill btn btn-success mt-2" onfocus="entrevistas.validarFechaHora()" type="submit" value="Agendar"/>
                </div>
            </form:form> 
        </div>
        <table class="table table-hover">
            <h4>Su agenda</h4>
            <thead>
                <tr>
                    <th>Nombre y apellido</th>
                    <th>Fecha y hora de la entrevista</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                <c:forEach items="${entrevistasAgendadas}" var="entrevista">
                    <tr class="pnt-js-entrevista">
                        <td>
                            <c:out value="${entrevista.nombreEntrevistado}"/>
                        </td>
                        <td>
                            <% 
                                Entrevista entrevista = (Entrevista)pageContext.getAttribute("entrevista");
                                ZonedDateTime fechaPactada = entrevista.getFechaPactada();
                                String fechaPactadaFormateada;
                        
                                if(fechaPactada != null) {
                                    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy - HH:mm");
                                    fechaPactadaFormateada = fechaPactada.format(formatter);    
                                }
                                else { 
                                    fechaPactadaFormateada = "A coordinar";
                                }
                        
                                out.println(fechaPactadaFormateada);
                            %>
                        </td>
                        <td>
                            <button class="pnt-js-boton-eliminar-entrevista btn btn-outline-danger" data-bs-toggle="modal" data-bs-target="#exampleModal" data-entrevista-id="${entrevista.id}">
                                <i class="fas fa-trash-alt"></i>
                            </button>
                        </td>
                    </tr>
                </c:forEach>
            </tbody>
        </table>
</main>

<!-- MODAL -->
<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Eliminar Entrevista</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <span>¿Está seguro que desea eliminar la entrevista?</span>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-outline-dark" data-bs-dismiss="modal">Cancelar</button>
                <button type="button" class="btn btn-danger pnt-js-boton-confirmar-eliminar" data-bs-dismiss="modal" id="eliminar">Eliminar</button>
            </div>
        </div>
    </div>
</div>

<!-- FIN MODAL -->

<script>
    <%
        String url = eliminarEntrevistaURL;
        String param = String.valueOf(System.currentTimeMillis());
    %>
    var url = "<%=url%>";
</script>
<script src="https://code.jquery.com/jquery-3.5.1.js" integrity="sha256-QWo7LDvxbWT2tbbQ97B53yJnYU3WhH/C8ycbRAkjPDc=" crossorigin="anonymous"></script>
<script src="https://kit.fontawesome.com/8cc86c3a0a.js" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW"
crossorigin="anonymous"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/page/entrevistas.js?param=${param}"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/service/entrevistaService.js?param=${param}"></script>
