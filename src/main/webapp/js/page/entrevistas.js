var entrevistas = (function () {

    const botonConfirmarEliminar = document.querySelector(".pnt-js-boton-confirmar-eliminar");
    const botonesEliminar = document.querySelectorAll(".pnt-js-boton-eliminar-entrevista");

    const iniciar = () => {
        botonesEliminar.forEach(function (boton) {
            boton.onclick = () => {
                botonConfirmarEliminar.onclick = () => {
                    const id = boton.getAttribute("data-entrevista-id");
                    const entrevistaAEliminar = boton.closest('.pnt-js-entrevista');
                    entrevistaService.eliminar(id)
                            .done(data => borrarEntrevista(entrevistaAEliminar))
                            .fail(message => alert("No se pudo completar la operacion"));
                };
            };
        });
    };

    const borrarEntrevista = (entrevista) => {
        entrevista.remove();
    };

    /* -------------------------------------------------------- */

    /* ------ Formulario: Validaciones de fecha y hora -------- */

    const validarFechaHora = () => {
        const fecha = document.querySelector("#fecha").value;
        const hora = document.querySelector("#hora");
        const fechaPactada = new Date(fecha);
        const hoy = new Date();

        // Si la fecha seleccionada por el usuario es igual a la de hoy,
        // se le asignara al horario a pactar un minimo que indica que este 
        // debe ser mayor al horario actual.
        // Caso contrario, elimina este atributo "min" para poder seleccionar 
        // cualquier horario

        if (fechasIguales(fechaPactada, hoy)) {
            hora.setAttribute("min", `${horaFormateada(hoy.getHours())}:${minutosFormateados(hoy.getMinutes() + 1)}`);
        } else {
            hora.removeAttribute("min");
        }

    };

    const fechasIguales = (fecha1, fecha2) => {
        // Compara si dos fechas son iguales basandose en la hora UTC
        // para evitar problemas de zona horaria

        return fecha1.getUTCDay() === fecha2.getUTCDay() &&
                fecha1.getUTCMonth() === fecha2.getUTCMonth() &&
                fecha1.getUTCFullYear() === fecha2.getUTCFullYear();
    };

    const horaFormateada = (horas) => {
        return (horas < 10) ? `0${horas}` : `${horas}`;
    };

    const minutosFormateados = (minutos) => {
        return (minutos < 10) ? `0${minutos}` : `${minutos}`;
    };

    /* -------------------------------------------------------- */

    return {
        init: iniciar,
        validarFechaHora: validarFechaHora
    };

})();

$(document).ready(entrevistas.init());

